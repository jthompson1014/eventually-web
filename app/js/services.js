'use strict';

/* Services */


var services = angular.module('services', []);

services.factory('EventService', function ($http, $q) {
	return {
		Get: function () {
			var deferred = $q.defer();

			$http.get('events/events.json').success(function(data) {
		    	deferred.resolve(data);
		    });

			return deferred.promise;
		}
	};
});