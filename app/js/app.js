'use strict';

/* App Module */

var app = angular.module('app', [
	'ngRoute',
	'controllers'
]);

app.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/events', {
				templateUrl: 'partials/event-list.html',
				controller: 'EventListController'
			}).
			otherwise({
				redirectTo: '/events'
			});
	}]);
