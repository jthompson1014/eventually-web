'use strict';

/* Controllers */

var controllers = angular.module('controllers', ['services']);

controllers.controller('EventListController', ['$scope', 'EventService', function($scope, EventService) {
	EventService.Get().then(function(data) {
		$scope.events = data;
	});
}]);

controllers.controller('MenuController', ['$scope', function ($scope) {
	$scope.rootUrl = '/app';
	$scope.menuItems = [
		{
			text: '+ Event'
		},
		{
			text: 'Manage Events',
		},
		{
			text: 'Settings'
		},
		{
			text: 'Log Off'
		}
	];
}]);
