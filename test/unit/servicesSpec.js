'use strict';

/* jasmine specs for services go here */

describe('EventService', function() {
	var $httpBackend, service;

	var events = [{name: 'A'}, {name: 'B'}];

	beforeEach(module('app'));
	beforeEach(inject(function($injector, _$httpBackend_) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('events/events.json').
				respond(events);

		service = $injector.get('EventService');
	}));

	it('should get all events', function () {
		var done = false;

		runs(function () {
			service.Get().then(function (results) {
				expect(results).toEqual(events);
			})
			.finally(function () {
				done = true;
			});

			$httpBackend.flush();
		});
		
		waitsFor(function () {
			return done;
		});
	});
});
