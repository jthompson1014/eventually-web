'use strict';

/* jasmine specs for controllers go here */



describe('EventListController', function() {
	var scope;

	var events = [{name: 'Nexus S'}, {name: 'Motorola DROID'}];


	beforeEach(module('app'));
	beforeEach(inject(function($rootScope, $controller, $q) {
		scope = $rootScope.$new();

		function GetMockEventService (events) {
			return {
				Get: function () {
					var deferred = $q.defer();
					deferred.resolve(events);
					return deferred.promise;
				}
			}
		}

		$controller('EventListController', {
			$scope: scope,
			EventService: GetMockEventService(events)
		});
	}));


	it('should create "events" model with all events from EventService', function() {
		scope.$apply();
		expect(scope.events).toEqual(events);
	});

});

describe('MenuController', function () {
	var scope;

	beforeEach(module('app'));
	beforeEach(inject(function($rootScope, $controller, $q) {
		scope = $rootScope.$new();

		$controller('MenuController', {
			$scope: scope
		});
	}));

	it('should provide the app root URL', function () {
		expect(scope.rootUrl).not.toBe(undefined);
	});

	it('should provide at least one menu item', function () {
		expect(scope.menuItems.length).toBeGreaterThan(0);
	})
});
